

(function ($) {
    "use strict";
 
    $(document).ready(function(){
        

        /*==================================
        01: Header
        ====================================*/
        $(window).on('scroll', function(e){
            if($(this).scrollTop() < 20){
                $('.main-header').removeClass('sticky')
            }else
                $('.main-header').addClass('sticky')        
        });


        /* 02: Offcanvas Menu
        ==============================================*/
        var windows = $(window);
        var screenSize = windows.width();

        $(".menu-trigger").on('click', function () {
            $(".offcanvas-menu").addClass('show');
            $(".shade").addClass('active');
            $(".main-header").addClass('offcanvasActive');
            $(".main-header").removeClass('sticky');
        });

        $(".offcanvas-cancel , .shade").on('click', function () {
            $(".offcanvas-menu").removeClass('show');
            $(".shade").removeClass('active');
            $(".main-header").removeClass('offcanvasActive');
            if( $(window).scrollTop() >= 100 ){
                $(".main-header").addClass("sticky");
            }
        });
        
        // Main Menu Apend To Offcanvas Menu
        if(screenSize<=991.98){
            $('.main-menu ul li.search-item').appendTo('.mobile-action-list');
            $('.main-menu ul li.cart-option').appendTo('.mobile-action-list');
            $('.main-menu ul li.account-option').appendTo('.mobile-action-list');
            $('.main-menu').appendTo('.offcanvas-menu .offcanvas-menu-inner');
        }

        $('.main-menu').find('ul li').parents('.main-menu ul li').addClass('has-sub-menu');
        $('.main-menu').find(".has-sub-menu").prepend('<span class="submenu-button"></span>'); 

        $('.offcanvas-menu-inner .main-menu').on("click", "li a , li.has-sub-menu , li .submenu-button" , function(e){
            
            if($(this).hasClass('has-submenu')){
                e.preventDefault();
                console.log($(this).siblings('ul'));
            }

            $(this).toggleClass("sub-menu-oppened");
            if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideUp();
            } else {
                $(this).siblings('ul').addClass('open').slideDown();
            }
             
        })

        
        /* 03: Cart Toggle
        =================================*/

        $('.cart-option').on('click', function () {
            if ($('[data-shipping]:checked').length > 0) {
                $('#shipping-address-form').slideDown();
            } else {
                $('#shipping-address-form').slideUp();
            }
        });

        /* 04: Search Toggle
        =================================*/

        $('.search-item a').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('.search-form').slideToggle('medium');
        });

        /*05: Check Data
        ====================================*/

        let checkData = function (data, value) {
            return typeof data === 'undefined' ? value : data;
        };


        /*06: Owl Carousel
        ====================================*/

        let $owlCarousel = $('.owl-carousel');
         
        $owlCarousel.each(function () {
            let $t = $(this);
                
            $t.owlCarousel({
                items: checkData( $t.data('owl-items'), 1 ),
                margin: checkData( $t.data('owl-margin'), 0 ),
                loop: checkData( $t.data('owl-loop'), true ),
                smartSpeed: 450,
                autoplay: checkData( $t.data('owl-autoplay'), false ),
                autoplayTimeout: checkData( $t.data('owl-speed'), 8000 ),
                center: checkData( $t.data('owl-center'), false ),
                animateOut: checkData( $t.data('owl-animate'), false ),
                autoHeight: checkData( $t.data('owl-auto-height'), false ),
                nav: checkData( $t.data('owl-nav'), false ),
                navText: ["<i class='fa fa-angle-right'></i>" , "<i class='fa fa-angle-left'></i>"],
                dots: checkData( $t.data('owl-dots'), false ),
                responsive: checkData( $t.data('owl-responsive'), {} )
            });
        });
        $('.product-list').trigger('refresh.owl.carousel');
        checkClasses();
        $owlCarousel.on('translated.owl.carousel', function(event) {
            checkClasses();
        });

        
        function checkClasses(){
            
                var total= $('.new-arrival-area .product-list .owl-stage .owl-item.active').length;
    
                $('.new-arrival-area .product-list .owl-stage .owl-item').removeClass('lastActiveItem');
                $('.feature-product-area .product-list .owl-stage .owl-item').removeClass('lastActiveItem');
                $('.sale-product-area .product-list .owl-stage .owl-item').removeClass('lastActiveItem');
                $('.sold-product-area .product-list .owl-stage .owl-item').removeClass('lastActiveItem');
    
                $('.new-arrival-area .product-list .owl-stage .owl-item.active').each(function(index){
                    if (index === 3) {
                        // this is the last one
                        $(this).addClass('lastActiveItem');
                    }
                });
                $('.feature-product-area .product-list .owl-stage .owl-item.active').each(function(index){
                    if (index === 3) {
                        // this is the last one
                        $(this).addClass('lastActiveItem');
                    }
                });

                $('.sale-product-area .product-list .owl-stage .owl-item.active').each(function(index){
                    if (index === 3) {
                        // this is the last one
                        $(this).addClass('lastActiveItem');
                    }
                });

                $('.sold-product-area .product-list .owl-stage .owl-item.active').each(function(index){
                    if (index === 3) {
                        // this is the last one
                        $(this).addClass('lastActiveItem');
                    }
                });
            
        }
        

        /* 07: Background Image
        ==============================================*/

        let bgImg = $('[data-bg-img]');

            bgImg.css('background', function(){
                return 'url(' + $(this).data('bg-img') + ') center center';
        });
        
        /* 08: Product Hover
        ==============================================*/

        const poductLinkImages = document.querySelectorAll('[data-img-src]');
        let imageSource = document.querySelector('#img-souce');
        const signleProduct = document.querySelectorAll('.single-product');
        const productDetails = document.querySelectorAll('.product-details');
        function selectDiv(e){
            imageSource = this.querySelector('#img-souce');
            
        }
        function showImage(){
            imageSource.src = this.dataset.imgSrc; 
            
        }

        
        if(poductLinkImages){
            poductLinkImages.forEach(image => image.addEventListener('mouseover',showImage));
        }
        
        if(signleProduct){
            signleProduct.forEach(product => product.addEventListener('mouseover',selectDiv));
        }
        if(productDetails){
            productDetails.forEach(productDetail => productDetail.addEventListener('mouseover',selectDiv));
        }
        
        $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
    });

    

        /* 09: Price Range
        =================================*/

        $('#price-range').slider({
            range: true,
            min: 0,
            max: 2000,
            values: [ 0, 970 ],
            slide: function( event, ui ) {
                $('#price-amount').val( '' + '$' + ui.values[ 0 ] + ' - $' + ui.values[ 1 ] );
            }
        });

        $('#price-amount').val( '' + '$' + $('#price-range').slider( 'values', 0 ) +
            ' - $' + $('#price-range').slider('values', 1 ) );

            $('.modal').on('shown.bs.modal', function (e) {
                $('.signle-quickView-product').resize();
            });

        /* 10: View Mode Change
        ==============================================*/
        $('.product-view-mode a').on('click', function (e) {
            e.preventDefault();

            var ProductBox = $('.product-box');
            var viewMode = $(this).data('target');

            $('.product-view-mode a').removeClass('active');
            $(this).addClass('active');
            if (viewMode == "list") {
                ProductBox.removeClass('column-self');
                ProductBox.addClass('poduct-list');
            } else {
                ProductBox.addClass('column-self');
                ProductBox.removeClass('poduct-list');
            }
            // ProductBox.removeClass('grid list').addClass(viewMode);
        });

        /* 11: Google Map
        ==============================================*/

        if($('#location-map').length){
            let googleMapSelector = $('#location-map');
            let myCenter = new google.maps.LatLng(40.6785635, -73.9664109);
            

            function initialize() {
                let mapProp = {
                    center: myCenter,
                    zoom: 11,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [
                            {
                                "featureType": "landscape.man_made",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ebebeb"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#d0e3b4"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural.terrain",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.business",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.medical",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#fbd3da"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#bde6ab"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "black"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station.airport",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#cfb2db"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#c7e5fd"
                                    }
                                ]
                            }
                        ]
                    };
                    
                    let map = new google.maps.Map(document.getElementById("location-map"),mapProp);
                }
                if (googleMapSelector.length) {
                    google.maps.event.addDomListener(window, 'load', initialize);
                }
            }
            

        /* 12: Back To Top 
        ==============================================*/

        let $backToTopBtn = $('.backToTop');

        if ($backToTopBtn.length) {
            let scrollTrigger = 350, // px
            backToTop = function () {
                let scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $backToTopBtn.addClass('show');
                } else {
                    $backToTopBtn.removeClass('show');
                }
            };

            backToTop();

            $(window).on('scroll', function () {
                backToTop();
            });

            $backToTopBtn.on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }

        const addButtons = document.querySelectorAll('.addon-plus');
        const cancelBtns = document.querySelectorAll('.cancel-btn');
        const shippingFeild = document.querySelector('.shipping-feild');
        const billingFeild = document.querySelector('.billing-feild');

        function FormActivtion(){
            if(this.dataset.relation == 'shipping'){
                shippingFeild.classList.add('form-active');
            }
            if(this.dataset.relation == 'billing'){
                billingFeild.classList.add('form-active');
                
            }
        }
        function removeForm(e){
            e.preventDefault();
            if($(e.target.parentElement.parentElement.parentElement.parentElement.parentElement).hasClass('shipping-feild')){
                shippingFeild.classList.remove('form-active');
            }
            if($(e.target.parentElement.parentElement.parentElement.parentElement.parentElement).hasClass('billing-feild')){
                billingFeild.classList.remove('form-active');
            } 
        }

        if(addButtons){
            addButtons.forEach(addButton => addButton.addEventListener('click', FormActivtion));
        }
        if(cancelBtns){
            cancelBtns.forEach(cancelBtn => cancelBtn.addEventListener('click', removeForm));
        }

        /* 13: Add To Cart 
        ==============================================*/

        const cartBtns = document.querySelectorAll('.add-cart');
        let totalQuantity = 0;
        let productList = JSON.parse(localStorage.getItem('productList')) || [];
        let actualProductPrice = 0;
        const cartList = document.querySelector(".cart-list");
        const cartTable = document.querySelector(".cart-list-table");
        const cartListParents = document.querySelectorAll(".cart-option");

        console.log(productList.length);
        function checkCart(){
            productList = JSON.parse(localStorage.getItem('productList')) || [];
            if(productList.length < 1 || productList == undefined){
                cartListParents.forEach(cartListParent => cartListParent.classList.add('cart-empty'));
                if(cartTable){
                    cartTable.classList.add('cart-empty');
                }
                
                console.log("empty");
            }else{
                cartListParents.forEach(cartListParent => cartListParent.classList.remove('cart-empty'));
                
                if(cartTable){
                    cartTable.classList.remove('cart-empty');
                }
                console.log("not empty");
            }
        }
        window.onload=checkCart();

        function buyProduct(e){
            e.preventDefault();
            const quantitySelectors = document.querySelectorAll('#qty');
            const product = e.target.parentElement.parentElement.parentElement.parentElement.parentElement;
            let poductId = this.dataset.cartId;
            console.log(product);
            let quantity = 1;
            const ProductInfo = {
                image: product.querySelector('.product-img img').src,
                productTitle: product.querySelector('.product-title').textContent,
                productPrice: product.querySelector('.product-price .new-price').textContent.replace("$" , ""),
                id: product.querySelector('.user-option .add-cart').getAttribute('data-cart-id'),
                quantity: 1
            }
            if(product.querySelector('#qty')){
                quantity = product.querySelector('#qty').value;
                ProductInfo.quantity = quantity;
            }
            let productList = JSON.parse(localStorage.getItem('productList')) || [];
            console.log(productList.length);
            if(productList.length < 1 || productList == undefined){
                productList.push(ProductInfo);
            }else{
                let alreadyExists = productList.filter(function(item){
                    return poductId === item.id
                }).length;
                if(alreadyExists < 1){
                    productList.push(ProductInfo);
                }
            }
            localStorage.setItem('productList',JSON.stringify(productList));
            fetchToCart(productList , cartList);
        }

        cartBtns.forEach(cartBtn => cartBtn.addEventListener('click', buyProduct));
        cartBtns.forEach(cartBtn => cartBtn.addEventListener('click', checkCart));

        const toastContent = document.querySelector('.toast-content p');
        function toastActive(){
            console.log(this.className);
            if(this.className == "add-cart"){
                toastContent.textContent = "Added To Cart";
            }
            $('.shop-toast').addClass('show');
            setTimeout(function(){
                $('.shop-toast').removeClass('show');
            }, 1000);

        }
        cartBtns.forEach(cart => cart.addEventListener('click', toastActive));

        function fetchToCart(productPlate = [] , cartList){
            
            const cartCount = document.querySelector('.cart-count');
            

            const cartTable = document.querySelector('.cart-list-table tbody');
            
            
            cartList.innerHTML = productPlate.map((content,i) => {
                
                totalQuantity += parseFloat(content.quantity.toString().replace("$" , ""));
                console.log(totalQuantity);
                actualProductPrice = (parseFloat(content.quantity.toString().replace("$" , "")) * parseFloat(content.productPrice.toString().replace("$","")));
                ;
                return `
                <li>
                    <div class="single-cart d-flex justify-content-between align-items-center">
                        <div class="product-img">
                            <a href="product-detail.html">
                                <img src="${content.image}" alt="Product Image">
                            </a>
                            
                        </div>
                        <div class="product-info text-left">
                            <h4 class="p-name">
                                <a href="product-detail.html">${content.productTitle}</a>
                            </h4>
                            <p class="price">$${actualProductPrice}</p>
                        </div>
                        <a data-id="${content.id}" href="#" class="delete-btn"><i class="fa fa-trash"></i></a>
                    </div>
                </li>
            `;
            }).join('');
            
            
            if(cartTable && productPlate.length != 0){
                console.log("empty cart");
                cartTable.innerHTML = productPlate.map((content,i) =>{
                    return `
                    <tr>
                        <td>
                        <a data-id="${content.id}" href="javascript:void(0)" class="delete-btn"><i class="fa fa-trash"></i></a>
                        </td>
                        <td class="product-image">
                            <img src="${content.image}" alt="Product-image">
                        </td>
                        <td>
                            ${content.productTitle}
                        </td>
                        <td>
                            <span class="currency-sign">৳</span>
                            <span class="u-price">${content.productPrice}</span>
                        </td>
                        <td>
                            <div class="quantity-selection d-flex">
                                <div class="product-quantity">
                                    <span class="shop--pro-qty">
                                        <a href="javascript:void(0)" class="dec shop--qty-btn-cart mr-1"><i class="fa fa-minus"></i></a>
                                        <input type="text" id="qty" value="${content.quantity}" class="" readonly>
                                        <a href="javascript:void(0)" class="inc shop--qty-btn-cart ml-1"><i class="fa fa-plus"></i></a>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <span class="currency-sign">$</span>
                            <span class="t-price">${actualProductPrice}</span>
                        </td>
                    </tr>
                `;
                }).join('');
            }else if(cartTable && productPlate.length == 0){
                cartTable.innerHTML = 
                `
                    <tr class="empty-text text-center">
                        <td colspan="6">
                            <p>Cart Is Empty</p>
                        </td>
                    </tr>
                `;
            }
            

            let totalCart = 0;
            for(let i = 0; i < productPlate.length; i++){
                totalCart = totalCart + 1;
            }
            const deleteBtns = document.querySelectorAll('.delete-btn');
            const spinnerBtns = document.querySelectorAll(".shop--qty-btn-cart");

            cartCount.innerHTML = totalCart;




            deleteBtns.forEach(deleteBtn => deleteBtn.addEventListener('click', removeProduct));
            deleteBtns.forEach(deleteBtn => deleteBtn.addEventListener('click', checkCart));
            deleteBtns.forEach(deleteBtn => deleteBtn.addEventListener('click', calculateGrandTotal));
            spinnerBtns.forEach(button => button.addEventListener('click', valueIncDec));
            if(cartTable){
                
                spinnerBtns.forEach(button => button.addEventListener('click', updateTotalPrice));
            }
            
            
            

            /* 14: Product Quantity
            ==============================================*/
            
            function valueIncDec(e){
                e.preventDefault();
                var $button = $(this);
                var oldValue = $button.parent().find('input').val();
                console.log(oldValue);
                if ($button.hasClass('inc')) {
                    var newVal = parseFloat(oldValue) + 1;
                    totalQuantity = totalQuantity + 1;

                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 0) {
                        var newVal = parseFloat(oldValue) - 1;
                        totalQuantity = totalQuantity - 1;
                    } else {
                        newVal = 0;
                    }
                }
                $button.parent().find('input').val(newVal);
            }
        }
            
        function removeProduct(e){
            e.preventDefault();
            let productId = this.dataset.id;
            productList = JSON.parse(localStorage.getItem('productList'));
            productList = productList.filter(product => product.id !== productId );
            localStorage.setItem('productList',JSON.stringify(productList));
            
            fetchToCart(productList , cartList);
        }

        fetchToCart(productList , cartList);

        /* 15: Cart Calculation
        ==============================================*/

        let unitPrice = 0;
        const discount = document.querySelector(".dis-amount");
        const vat = document.querySelector(".vat-amount");
        const unitDelCharge = document.querySelector(".unit-charge");
        const totalDelCharge = document.querySelector(".total-charge");
        const deliverySelect = document.getElementById('dl-charge'); 
        function updateTotalPrice(e){
            console.log(e);
            let row = $(this).closest('tr');
            let unitPrice = row.find('.u-price').text();
            let quantity = row.find('#qty').val();
            console.log((parseFloat(unitPrice.toString().replace("$" , ""))));
            let totalPrice = row.find('.t-price').text((parseFloat(unitPrice.toString().replace("$" , "")) * (parseFloat(quantity.toString().replace("$" , ""))).toFixed(3)));
            calculateGrandTotal();
        }
        function calculateGrandTotal(){
            let  grandTotal = 0;
            let vatAmount = 0;
            let totalDelCharge = 0;
            let deliveryCharge = 0;
            // let deliveryType = this.options[this.selectedIndex];
            if(this != undefined && this.classList.contains('dl-charge')){
                
                deliveryCharge = this.options[this.selectedIndex].value;
            }else{
                if(deliverySelect){
                    deliveryCharge = deliverySelect.options[deliverySelect.selectedIndex].value;
                }
                
            }
            console.log(deliveryCharge);
            $('.t-price').each(function(i){
                let price = Number($(this).html());
                if(price >= 0){
                    grandTotal += price;
                }
            });
            
            if(discount){
                
                grandTotal = (grandTotal - Number(discount.textContent));
                if(grandTotal <=0){
                    grandTotal = 0;
                }
                console.log(grandTotal);
            }
            if(vat){
                vatAmount = (grandTotal * (Number(vat.textContent) / 100 ));
                console.log(vatAmount);
            }
            
            totalDelCharge = (totalQuantity * (Number(deliveryCharge)));
            
            grandTotal += vatAmount;
            grandTotal += totalDelCharge;
            if(grandTotal <=0){
                grandTotal = 0;
            }
            $('.grand-total').html(grandTotal.toFixed(3));
            $('.total-charge').html(totalDelCharge.toFixed(3));
            $('.total-vat').html(vatAmount.toFixed(3));
            $('.charge-quantity').html(totalQuantity);
            $('.unit-charge').html(deliveryCharge);
        }
        calculateGrandTotal();
        if(deliverySelect){
            deliverySelect.addEventListener("change" , calculateGrandTotal);
        }


        /* 16: Start Rating
        ==============================================*/
        $('.rating li').on('mouseover' ,  function(){
            console.log(this.dataset);
            let onStar = parseInt($(this).data('star'), 10);

            $(this).parent().children('li').each(function(e){
                if (e < onStar) {
                    $(this).addClass('fill');
                }
                else {
                    $(this).removeClass('fill');
                }
                });
        }).on('mouseout', function(){
            $(this).parent().children('li').each(function(e){
                $(this).removeClass('fill');
            });
            });


        /* 17: Profile Editing
        ==============================================*/
            const editBtns = document.querySelectorAll('.edit-btn');
            const proCancelBtns = document.querySelectorAll('.cancel-btn');

            function activeEditMode(e){
                e.preventDefault();
                const selectTab = this.parentElement.parentElement.parentElement.parentElement;
                selectTab.classList.add('editMode')
            }

            function activeViewMode(e){
                e.preventDefault();
                const selectTab = this.parentElement.parentElement.parentElement.parentElement;
                selectTab.classList.remove('editMode')
            }

            if(editBtns){
                editBtns.forEach(button => button.addEventListener('click',activeEditMode));
            }

            if(proCancelBtns){
                proCancelBtns.forEach(button => button.addEventListener('click',activeViewMode));
            }


            /* 18: Profile Image 
            ==============================================*/

            var readURL = function(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.pro-imgSrc').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".pro-upload").on('change', function(){
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".pro-upload").click();
        });


        // Table Row Remove 
        const removeBtns = document.querySelectorAll(".cross-btn");

        // Remove The Table Row
        function removeRow(e){
            e.preventDefault();
            let row = this.parentNode.parentNode;
            row.parentNode.removeChild(row);
        }

        removeBtns.forEach(removeBtn => removeBtn.addEventListener('click', removeRow));
        removeBtns.forEach(removeBtn => removeBtn.addEventListener('click', updateOrderTable));

        // const orderUnitPrice = document.querySelectorAll('.o-unit-price');


        function updateOrderTable(){
            const orderTable  = document.querySelector('.order-info-table tbody');
            if(orderTable){
                const orderDiscount = document.querySelector('.o-discount');
                const orderVat = document.querySelector('.o-vat');
                const returnChargePercentage = document.querySelector('.return-charge-p');
                const returnChargeAmount = document.querySelector('.return-charge-am');
                const orderUnitCharge = document.querySelector('.o-del-charge');
                const oTotalCharge = document.querySelector('.order-charge');
                const allRows = orderTable.querySelectorAll('tr');


                let orderGrandTotal = 0;
                let orderVatValue = 0;
                let returnCharge = 0;
                let totalOrderCharge = 0;
                let totalOrderQuantity = 0;
                
                for(let i = 0; i < allRows.length; i++){
                    let  orderUnitPrice = 0;
                    let  orderUnitQuantity = 0;
                    let totalOrder = 0;
                    orderUnitPrice = allRows[i].getElementsByClassName('o-unit-price')[0];
                    orderUnitQuantity = allRows[i].getElementsByClassName('o-quantity')[0];
                    totalOrder = (Number(orderUnitPrice.innerText) * (Number(orderUnitQuantity.innerText)));
                    $('.o-unit-total').html(totalOrder);
                    
                    }

                    $('.o-unit-total').each(function(i){
                        
                        let price = Number($(this).html());
                        if(price >= 0){
                            orderGrandTotal += price;
                        }
                    });

                    $('.o-quantity').each(function(i){
                        
                        let orderQuantity = Number($(this).html());
                        totalOrderQuantity += orderQuantity;
                        $('.total-order').html(totalOrderQuantity);
                        
                    });

                    if(orderUnitCharge){
                        totalOrderCharge = (Number(totalOrderQuantity) * (Number(orderUnitCharge.textContent)));
                        $('.order-charge').html(totalOrderCharge);
                    }

                    if(orderDiscount){
                    orderGrandTotal = (orderGrandTotal - Number(orderDiscount.textContent));
                    if(orderGrandTotal <=0){
                        orderGrandTotal = 0;
                    }
                    
                }
                if(orderVat){
                    orderVatValue = (orderGrandTotal * (Number(orderVat.textContent) / 100 ));
                    $('.o-vat-amount').html(orderVatValue);
                }
                if(returnChargePercentage){
                    returnCharge = (orderGrandTotal * (Number(returnChargePercentage.textContent) / 100 ));
                    $('.return-charge-am').html(returnCharge);
                    orderGrandTotal = (orderGrandTotal - Number(returnChargeAmount.textContent));
                }
                if(oTotalCharge){
                    orderGrandTotal = (orderGrandTotal + Number(oTotalCharge.textContent));
                }
                $('.o-grand-total').html(orderGrandTotal);
            }
            
        }
        updateOrderTable();
        

    })(jQuery);